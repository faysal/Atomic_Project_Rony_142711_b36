<?php
namespace App\Birthday;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;

class Birthday extends DB{
    public $id;
    public $user_name;
    public $birth_date;

    public function __construct(){
        parent::__construct();
    }
    public function setData($postVariableData=NULL){
        /*$this->book_title = $postVariableData['book_name'];
        $this->author_name = $postVariableData['author_name'];
        $this->price = $postVariableData['price'];*/
        if(array_key_exists("id",$postVariableData)){
            $this->id = $postVariableData['id'];
        }
        if(array_key_exists("user_name",$postVariableData)){
            $this->user_name = $postVariableData['user_name'];
        }
        if(array_key_exists("birth_date",$postVariableData)){
            $this->birth_date = $postVariableData['birth_date'];
        }
    }

    public function store(){
        /*$sql = "insert into birthday(name,birth_date)
                VALUES ('$this->user_name', '$this->birth_date')";

        echo $sql;
        die();*/

        $arrData = array($this->user_name,$this->birth_date);
        $sql = "insert into birthday(name,birth_date)
                VALUES (?, ?)";
        $STH = $this->DBH->prepare($sql);  //prepare() object return kore
        $result = $STH->execute($arrData);
        //Utility::dd($result);      // arporer codegula run hbe na
        if($result){
            //Message::setMessage("data has been inserted succesfully");
            Message::message("data has been inserted succesfully");
        }else {
            //Message::setMessage("Failed! data has not been inserted");
            Message::message("Failed! data has not been inserted");
        }
        Utility::redirect('index.php');
        //header('Location:create.php');
    }
    
    public function index($fetchMode="ASSOC"){
        $sql = "SELECT * FROM birthday WHERE is_deleted='NO'";
        $STH = $this->DBH->query($sql);
        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,"OBJ")>0){
            $STH->setFetchMode(PDO::FETCH_OBJ);
        }else{
            $STH->setFetchMode(PDO::FETCH_ASSOC);
        }
        $allData = $STH->fetchAll();
        return $allData;
    }

    public function view($fetchMode="ASSOC"){
        $sql = "SELECT * FROM birthday WHERE id=".$this->id;
        $STH = $this->DBH->query($sql);
        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,"OBJ")>0){
            $STH->setFetchMode(PDO::FETCH_OBJ);
        }else{
            $STH->setFetchMode(PDO::FETCH_ASSOC);
        }
        $oneData = $STH->fetch();
        return $oneData;
    }
    
    public function show($fetchMode="ASSOC"){
        $sql = "SELECT * FROM birthday WHERE id=".$this->id;
        $STH = $this->DBH->query($sql);
        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,"OBJ")>0){
            $STH->setFetchMode(PDO::FETCH_OBJ);
        }else{
            $STH->setFetchMode(PDO::FETCH_ASSOC);
        }
        $onedata = $STH->fetch();
        return $onedata;
    }

    public function update(){
        $arrdata = array($this->user_name,$this->birth_date);
        $sql = "UPDATE birthday SET name=?, birth_date=? WHERE id=".$this->id;
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrdata);
        if($result){
            Utility::redirect('index.php');
        }else{
            Message::message("Sorry!.......");
        }
    }

    public function trash_delete(){
        $sql = "UPDATE birthday SET is_deleted=NOW() WHERE id=".$this->id;
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute();
        Utility::redirect('index.php');
    }

    public function delete(){
        $sql = "DELETE FROM birthday WHERE id=".$this->id;
        $STH = $this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect('index.php');
    }

    public function trashed_list($fetchMode="ASSOC"){
        $sql = "SELECT * FROM birthday WHERE is_deleted!='NO'";
        $STH = $this->DBH->query($sql);
        $fetchMode = strtoupper($fetchMode);
        if(substr_count("OBJ",$fetchMode)>0){
            $STH->setFetchMode(PDO::FETCH_OBJ);
        }else{
            $STH->setFetchMode(PDO::FETCH_ASSOC);
        }
        $allData = $STH->fetchAll();
        return $allData;
    }

    public function recover(){
        $sql = "UPDATE birthday SET is_deleted='NO' WHERE id=".$this->id;
        $STH = $this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect('trashed_list.php');
    }

    public function indexPaginator($page=1,$itemsPerPage=3){
        $start = (($page-1) * $itemsPerPage);
        $sql = "SELECT * from birthday WHERE is_deleted = 'No' LIMIT $start,$itemsPerPage";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }// end of indexPaginator();
}