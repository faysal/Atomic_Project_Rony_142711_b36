<?php
namespace App\Model;
use PDO;
class Database{
    public $DBH;
    public $username="root";
    public $password="";
    public function __construct(){
        try{
            $this->DBH = new PDO("mysql:host=localhost;dbname=atomic_project_b36",
                $this->username,$this->password);
        }
        catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
}