<?php
namespace App\Profile_Picture;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;


class Profile_Picture extends DB{
    public $id;
    public $name;
    public $image_name;

    public function __construct(){
        parent::__construct();
    }

    public function setData($postVariableData=NULL){
        if(array_key_exists("id",$postVariableData)){
            $this->id = $postVariableData['id'];
        }
        if(array_key_exists("name",$postVariableData)){
            $this->name = $postVariableData['name'];
        }
        if(array_key_exists("image_name",$postVariableData)){
            $this->image_name = $postVariableData['image_name'];
        }
    }

    public function store(){
        $arrData = array($this->name,$this->image_name);
        $sql = "insert into profile_image(name,profile_picture)
                VALUES (?, ?)";
        $STH = $this->DBH->prepare($sql);  //prepare() object return kore
        $result = $STH->execute($arrData);
        //Utility::dd($result);      // arporer codegula run hbe na
        if($result){
            //Message::setMessage("data has been inserted succesfully");
            Message::message("data has been inserted succesfully");
        }else {
            //Message::setMessage("Failed! data has not been inserted");
            Message::message("Failed! data has not been inserted");
        }
        Utility::redirect('index.php');
        //header('Location:create.php');
    }

    public function index($fetchMode="ASSOC"){
        $sql = "SELECT * FROM profile_image WHERE is_deleted='NO'";
        $STH = $this->DBH->query($sql);
        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,"OBJ")>0){
            $STH->setFetchMode(PDO::FETCH_OBJ);
        }else{
            $STH->setFetchMode(PDO::FETCH_ASSOC);
        }
        $alldata = $STH->fetchAll();
        return $alldata;
    }

    public function show($fetchMode="ASSOC"){
        $sql = "SELECT * FROM profile_image WHERE id=".$this->id;
        $STH = $this->DBH->query($sql);
        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,"OBJ")>0){
            $STH->setFetchMode(PDO::FETCH_OBJ);
        }else{
            $STH->setFetchMode(PDO::FETCH_ASSOC);
        }
        $alldata = $STH->fetch();
        return $alldata;
    }

    public function update(){
        $arrData = array ($this->name,$this->image_name);
        $sql = "UPDATE profile_image SET name=?, profile_picture=? WHERE id=".$this->id;
        $STH= $this->DBH->prepare($sql);
        $STH->execute($arrData);
        Utility::redirect('index.php');
    }

    public function update1(){
        if($this->image_name=""){
            $sql = "UPDATE profile_image SET name=? WHERE id=".$this->id;
            $STH= $this->DBH->prepare($sql);
            $STH->execute();
            Utility::redirect('index.php');
        }else{
            $arrData = array($this->name, $this->image_name);
            $sql = "UPDATE profile_image SET name=?, profile_picture=? WHERE id=".$this->id;
            $STH= $this->DBH->prepare($sql);
            $STH->execute($arrData);
            Utility::redirect('index.php');
        }
        $sql = "UPDATE profile_image SET name=?, profile_picture=? WHERE id=".$this->id;
        $STH= $this->DBH->prepare($sql);
        $STH->execute($arrData);
        Utility::redirect('index.php');
    }

    public function trash_delete(){
        $sql = "UPDATE profile_image SET is_deleted=NOW() WHERE id=".$this->id;
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute();
        Utility::redirect('index.php');
    }

    public function delete(){
        $sql = "DELETE FROM profile_image WHERE id=".$this->id;
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute();
        Utility::redirect('index.php');
    }
}
