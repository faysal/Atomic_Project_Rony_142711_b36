<?php
namespace App\Hobbies;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;


class Hobbies extends DB{
    public $id;
    public $name;
    public $hobbies;

    public function __construct(){
        parent::__construct();
    }

    public function setData($postVariableData=NULL){
        if(array_key_exists("id",$postVariableData)){
            $this->id = $postVariableData['id'];
        }
        if(array_key_exists("name",$postVariableData)){
            $this->name = $postVariableData['name'];
        }
        if(array_key_exists("hobbies",$postVariableData)){
            $str = implode(",",$_POST['hobbies']);
            $this->hobbies = $str;
        }
    }

    public function store(){
        $arrData = array($this->name,$this->hobbies);
        $sql = "insert into hobbies(name,hobbies)
                VALUES (?, ?)";
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);
        //Utility::dd($result);      // arporer codegula run hbe na
        if($result){
            //Message::setMessage("data has been inserted succesfully");
            Message::message("data has been inserted succesfully");
        }else {
            //Message::setMessage("Failed! data has not been inserted");
            Message::message("Failed! data has not been inserted");
        }
        Utility::redirect('index.php');
        //header('Location:create.php');
    }

    public function index($fetchMode="ASSOC"){
        $sql = "SELECT * FROM hobbies WHERE is_deleted='NO'";
        $STH = $this->DBH->query($sql);
        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,"OBJ")>0){
            $STH->setFetchMode(PDO::FETCH_OBJ);
        }else{
            $STH->setFetchMode(PDO::FETCH_ASSOC);
        }
        $alldata = $STH->fetchAll();
        return $alldata;
    }

    public function show($fetchMode="ASSOC"){
        $sql = "SELECT * FROM hobbies WHERE id=".$this->id;
        $STH = $this->DBH->query($sql);
        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,"OBJ")>0){
            $STH->setFetchMode(PDO::FETCH_OBJ);
        }else{
            $STH->setFetchMode(PDO::FETCH_ASSOC);
        }
        $onedata = $STH->fetch();
        return $onedata;
    }

    public function update(){
        $arrData = array ($this->name,$this->hobbies);
        $sql = "UPDATE hobbies SET name=?, hobbies=? WHERE id=".$this->id;
        $STH= $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);
        Utility::redirect('index.php');
    }

    public function trash_delete(){
        $sql = "UPDATE hobbies SET is_deleted=NOW() WHERE id=".$this->id;
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute();
        Utility::redirect('index.php');
    }

    public function show_trashed_list($fetchMode="OBJ"){
        $sql = "SELECT * FROM hobbies WHERE is_deleted!='NO'";
        $STH = $this->DBH->query($sql);
        $fetchMode = strtoupper($fetchMode);
        if(substr_count("OBJ",$fetchMode)>0){
            $STH->setFetchMode(PDO::FETCH_OBJ);
        }else{
            $STH->setFetchMode(PDO::FETCH_ASSOC);
        }
        $allData = $STH->fetchAll();
        return $allData;
    }

    public function recover(){
        $sql = "UPDATE hobbies SET is_deleted='NO' WHERE id=".$this->id;
        $STH = $this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect('trashed_list.php');
    }

    public function delete(){
        $sql = "DELETE FROM hobbies WHERE id=".$this->id;
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute();
        Utility::redirect('index.php');
    }
}
