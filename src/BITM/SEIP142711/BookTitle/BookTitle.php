<?php
namespace App\BookTitle;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;

class BookTitle extends DB{
    public $id;
    public $book_title;
    public $author_name;
    public $price;

    public function __construct(){
        parent::__construct();
        if(!isset($_SESSION)){
            session_start();
        }
    }
    public function setData($postVariableData=NULL){
        /*$this->book_title = $postVariableData['book_name'];
        $this->author_name = $postVariableData['author_name'];
        $this->price = $postVariableData['price'];*/
        if(array_key_exists("id",$postVariableData)){
            $this->id = $postVariableData['id'];
        }
        if(array_key_exists("book_title",$postVariableData)){
            $this->book_title = $postVariableData['book_title'];
        }
        if(array_key_exists("author_name",$postVariableData)){
            $this->author_name = $postVariableData['author_name'];
        }
        if(array_key_exists("price",$postVariableData)) {
            $this->price = $postVariableData['price'];
        }
    }

    public function store(){
        $arrData = array($this->book_title,$this->author_name,$this->price);
        $sql = "insert into book_title(book_title,author_name,price)
                VALUES (?, ?, ?)";
        $STH = $this->DBH->prepare($sql);  //prepare() object return kore
        $result = $STH->execute($arrData);
        //Utility::dd($result);      // arporer codegula run hbe na
        if($result){
            //Message::setMessage("data has been inserted succesfully");
            Message::message("data has been inserted succesfully");
        }else {
            //Message::setMessage("Failed! data has not been inserted");
            Message::message("Failed! data has not been inserted");
        }
        Utility::redirect('index.php');
        //header('Location:create.php');
    }

    public function index($fetchMode="ASSOC"){
        $sql = "SELECT * FROM book_title WHERE is_deleted='NO'";
        $STH = $this->DBH->query($sql);
        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ')>0){
            $STH->setFetchMode(PDO::FETCH_OBJ);
        }else{
            $STH->setFetchMode(PDO::FETCH_ASSOC);
        }
        $allData = $STH->fetchAll();
        return $allData;
    }

    public function view($fetchMode="ASSOC"){
        $STH = $this->DBH->query('SELECT * FROM book_title WHERE id='.$this->id);
        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ')>0){
            $STH->setFetchMode(PDO::FETCH_OBJ);
        }else{
            $STH->setFetchMode(PDO::FETCH_ASSOC);
        }
        $oneData = $STH->fetch();
        return $oneData;
    }

    public function show($fetchMode="ASSOC"){
        $STH = $this->DBH->query('SELECT * FROM book_title WHERE id='.$this->id);
        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ')>0){
            $STH->setFetchMode(PDO::FETCH_OBJ);
        }else{
            $STH->setFetchMode(PDO::FETCH_ASSOC);
        }
        $oneData = $STH->fetch();
        return $oneData;
    }

    public function update(){
        $arrData = array($this->book_title,$this->author_name,$this->price);
        $sql = 'UPDATE book_title SET book_title=?, author_name=?, price=? WHERE id='.$this->id;
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);
        Utility::redirect('index.php');
    }
    
    public function trash_delete(){
        $sql = 'UPDATE book_title SET is_deleted=NOW() WHERE id='.$this->id;
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute();
        Utility::redirect('index.php');
    }

    public function show_trashed_list($fetchMode="ASSOC"){
        $sql = "SELECT * FROM book_title WHERE is_deleted!='NO'";
        $STH = $this->DBH->query($sql);
        $fetchMode = strtoupper($fetchMode);
        if(substr_count("OBJ",$fetchMode)>0){
            $STH->setFetchMode(PDO::FETCH_OBJ);
        }else{
            $STH->setFetchMode(PDO::FETCH_ASSOC);
        }
        $allData = $STH->fetchAll();
        return $allData;
    }
    
    public function recover(){
        $sql = "UPDATE book_title SET is_deleted='NO' WHERE id=".$this->id;
        $STH = $this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect('trashed_list.php');
    }
    
    public function delete(){
        $sql = "DELETE FROM book_title WHERE id=".$this->id;
        $STH = $this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect('index.php');
    }

    public function indexPaginator($page=1,$itemsPerPage=3){
        $start = (($page-1) * $itemsPerPage);
        $sql = "SELECT * from book_title  WHERE is_deleted = 'No' LIMIT $start,$itemsPerPage";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }// end of indexPaginator();
}