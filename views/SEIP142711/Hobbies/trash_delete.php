<?php
require_once ("../../../vendor/autoload.php");
use App\Hobbies\Hobbies;
if(!isset($_SESSION)){
    session_start();
}

$objHobbies = new Hobbies();
$objHobbies->setData($_GET);
$objHobbies->trash_delete();