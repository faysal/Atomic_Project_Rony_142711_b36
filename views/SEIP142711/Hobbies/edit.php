<?php
require_once ("../../../vendor/autoload.php");
use App\Hobbies\Hobbies;
use App\Message\Message;
if(!isset($_SESSION)){
    session_start();
}
$objHobbies = new Hobbies();
$objHobbies->setData($_GET);
$onedata = $objHobbies->show("obj");
/*var_dump($onedata);*/
$arrlist = explode(",",$onedata->hobbies);
/*print_r($arrlist);*/


?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hobbies</title>

    <!--sidebar links-->
    <link rel="stylesheet" href="../../../resource/Birthday_assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/Birthday_assets/css/font-awesome.min.css">
    <link href="../../../resource/Birthday_assets/css/main.css" rel="stylesheet">
    <!-- sidebar links end -->

    <!-- form links start -->
    <script src="../../../resource/Birthday_assets/js/bootstrap.min.js"></script>
    <script src="../../../resource/Birthday_assets/js/jquery-1.11.1.min.js"></script>
    <script src="../../../resource/Birthday_assets/js/jquery.min.js"></script>
    <link href="../../../resource/Birthday_assets/css/bootstrap-theme.css" rel="stylesheet">
    <!-- form links end -->
</head>
<body>
<header style="background-color: midnightblue;width: 100%;text-align: left;position: fixed;">
    <h3 style="color: white;margin: 0;padding: 1% 5%;">Atomic Project</h3>
</header>
<div id="wrapper">
    <aside id="sideBar" style="margin-top: 4%;">
        <ul class="main-nav">
            <li>
                <a href="../BookTitle/create.php">- Book Title</a>
            </li>
            <li>
                <a href="../Birthday/create.php">- Birthday</a>
            </li>
            <li>
                <a href="../City/create.php">- City</a>
            </li>
            <li>
                <a href="../Email/create.php">- Email</a>
            </li>
            <li>
                <a href="../Gender/create.php">- Gender</a>
            </li>
            <li>
                <a href="create.php">- Hobbies</a>
            </li>
            <li>
                <a href="../Profile_Picture/create.php">- Profile Picture</a>
            </li>
            <li>
                <a href="../Summary_Of_Organization/create.php">- Summary of Organization</a>
            </li>
        </ul>
    </aside>
</div>


<form action="update.php" method="post" class="form-horizontal" style="padding-top: 10%;" >
    <fieldset>

        <!-- Form Name -->
        <legend><h3 class="text-center">Hobbies Form</h3></legend>

        <!-- name-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="id">ID</label>
            <div class="col-md-4">
                <input type="text" id="id" name="id" value="<?php echo $onedata->id; ?>" class="form-control input-md" readonly="readonly">
            </div>
        </div>

        <!-- name-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="name">Name</label>
            <div class="col-md-4">
                <input type="text" id="name" name="name" value="<?php echo $onedata->name; ?>" class="form-control input-md" required="required">
            </div>
        </div>

        <!-- city-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="hobbies">Hobbies</label>
            <div class="col-md-4">
                <input type="checkbox" id="hobbies" name="hobbies[]" value="playing" <?php
                                                                                        if(in_array("playing",$arrlist)){
                                                                                            echo "checked";
                                                                                        }
                                                                                    ?> >  Playing<br>
                <input type="checkbox" id="hobbies" name="hobbies[]" value="tv" <?php
                                                                                        if(in_array("tv",$arrlist)){
                                                                                            echo "checked";
                                                                                        }
                                                                                     ?> >  Seeing TV<br>
                <input type="checkbox" id="hobbies" name="hobbies[]" value="movie" <?php
                                                                                        if(in_array("movie",$arrlist)){
                                                                                            echo "checked";
                                                                                        }
                                                                                    ?> >  seeing movie<br>
                <input type="checkbox" id="hobbies" name="hobbies[]" value="travelling" <?php
                                                                                            if(in_array("travelling",$arrlist)){
                                                                                                echo "checked";
                                                                                            }
                                                                                        ?> >  travelling
            </div>
        </div>

        <!-- Button -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="save"></label>
            <div class="col-md-4">
                <button type="submit" id="submit" name="submit" class="btn btn-info">update</button>
            </div>
        </div>
        <a style="margin-left: 435px;text-decoration: none" href="index.php">show list</a>
    </fieldset>
</form>
</body>
</html>