<?php
require_once ("../../../vendor/autoload.php");
use App\Gender\Gender;
if(!isset($_SESSION)){
    session_start();
}
$objGender = new Gender();
$objGender->setData($_POST);
$objGender->update();