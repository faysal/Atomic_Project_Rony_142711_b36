<?php
require_once ("../../../vendor/autoload.php");
use App\Gender\Gender;
if(!isset($_SESSION)){
    session_start();
}
$objGender = new Gender();
$alldata = $objGender->show_trashed_list("obj");
?>

<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/Birthday_assets/js/jquery.min.js"></script>
    <script src="../../../resource/Birthday_assets/js/bootstrap.min.js"></script>
</head>
<body>
<header style="text-align: center">
    <h3>Email Trashed List</h3>
</header>
<table class="table table-striped" style="width: 50%;margin: 2% auto;">
    <thead>
    <tr>
        <th>Serial No.</th>
        <th>Id</th>
        <th>Name</th>
        <th>Gender</th>
        <th>Trashed Date</th>
        <th>Trashed Time</th>
        <th>Action</th>
    </tr>
    </thead>

    <?php
    $serial = 1;
    foreach($alldata as $oneData){
        $arr = explode(" ", $oneData->is_deleted);
        $date = $arr[0];
        $time = $arr[1];
        ?>
        <tr class="table">
            <td><?php echo $serial; ?></td>
            <td><?php echo $oneData->id; ?></td>
            <td><?php echo $oneData->name; ?></td>
            <td><?php echo $oneData->gender; ?></td>
            <td><?php echo $date; ?></td>
            <td><?php echo $time; ?></td>
            <td>
                <a href="recover.php?id=<?php echo $oneData->id; ?>"><button class="btn btn-primary">recover</button></a>
            </td>
        </tr>
        <?php
        $serial++;
    }
    ?>
</table>
<div  style="text-align: center">
    <a href="index.php"><button class="btn btn-default">back</button></a>
</div>
</body>
</html>

