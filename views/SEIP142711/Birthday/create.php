<?php
require_once ("../../../vendor/autoload.php");
use App\Message\Message;
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Birthday</title>

    <link rel="stylesheet" href="../../../resource/Birthday_assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/Birthday_assets/css/font-awesome.min.css">
    <link href="../../../resource/Birthday_assets/css/main.css" rel="stylesheet">
    <!-- Bootstrap JavaScript -->
    <script src="../../../resource/Birthday_assets/js/bootstrap.min.js"></script>
    <!-- jQuery -->
    <script src="../../../resource/Birthday_assets/js/jquery-1.11.1.min.js"></script>
    <script src="../../../resource/Birthday_assets/js/jquery.min.js"></script>
    
    
    
    <!-- Bootstrap -->
    <!--<link href="../../../resource/Birthday_assets/css/bootstrap.min.css" rel="stylesheet">-->
    <link href="../../../resource/Birthday_assets/css/bootstrap-theme.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="../../../resource/Birthday_assets/js/html5shiv.min.js"></script>
    <script src="../../../resource/Birthday_assets/js/respond.min.js"></script>
    
    <![endif]-->
</head>
<body background="../../../img/12.jpg">
<header style="background-color: black;width: 100%;text-align: left;position: fixed;">
    <h3 style="color: white;margin: 0;padding: 1% 5%;">Atomic Project</h3>
</header>

<div id="wrapper">
    <aside id="sideBar" style="margin-top: 4%">
        <ul class="main-nav">
            <li>
                <a href="../BookTitle/index.php">- Book Title</a>
            </li>
            <li>
                <a href="index.php">- Birthday</a>
            </li>
            <li>
                <a href="../City/index.php">- City</a>
            </li>
            <li>
                <a href="../Email/index.php">- Email</a>
            </li>
            <li>
                <a href="../Gender/index.php">- Gender</a>
            </li>
            <li>
                <a href="../Hobbies/index.php">- Hobbies</a>
            </li>
            <li>
                <a href="../Profile_Picture/index.php">- Profile Picture</a>
            </li>
            <li>
                <a href="../Summary_Of_Organization/index.php">- Summary of Organization</a>
            </li>
        </ul>
    </aside>
</div>






<form action="store.php" method="post" class="form-horizontal" style="padding-top: 10%;">
    <fieldset>

        <!-- Form Name -->
        <legend><h3 class="text-center">Birthday Form</h3></legend>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="user_name">Name</label>
            <div class="col-md-4">
                <input type="text" id="user_name" name="user_name" placeholder="Enter your name...." class="form-control input-md" required="required">
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="birth_date">Birth date</label>
            <div class="col-md-4">
                <input type="date" id="birth_date" name="birth_date" placeholder="select birth date...." class="form-control input-md" required="required">

            </div>
        </div>

        <!-- Button -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="save"></label>
            <div class="col-md-4">
                <button type="submit" id="submit" name="submit" class="btn btn-info">save</button>
            </div>
        </div>
        <div id="Mynote" style="margin-left: 34%;">
            <p><b><?php echo Message::message(); ?></b></p>
        </div>
    </fieldset>
</form>
<a style="color: black;margin-left: 435px;text-decoration: none" href="index.php">show list</a>
<script>
    $(document).ready(function () {
        $("#Mynote").fadeOut("slow");
    })
</script>
</body>
</html>