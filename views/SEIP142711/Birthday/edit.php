<?php
require_once ("../../../vendor/autoload.php");
use App\Birthday\Birthday;
use App\Message\Message;
if(!isset($_SESSION)){
    session_start();
}
$objBirthday = new Birthday();
$objBirthday->setData($_GET);
$onedata = $objBirthday->show("obj");
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Birthday</title>

    <link rel="stylesheet" href="../../../resource/Birthday_assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/Birthday_assets/css/font-awesome.min.css">
    <link href="../../../resource/Birthday_assets/css/main.css" rel="stylesheet">
    <!-- Bootstrap JavaScript -->
    <script src="../../../resource/Birthday_assets/js/bootstrap.min.js"></script>
    <!-- jQuery -->
    <script src="../../../resource/Birthday_assets/js/jquery-1.11.1.min.js"></script>
    <script src="../../../resource/Birthday_assets/js/jquery.min.js"></script>



    <!-- Bootstrap -->
    <!--<link href="../../../resource/Birthday_assets/css/bootstrap.min.css" rel="stylesheet">-->
    <link href="../../../resource/Birthday_assets/css/bootstrap-theme.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="../../../resource/Birthday_assets/js/html5shiv.min.js"></script>
    <script src="../../../resource/Birthday_assets/js/respond.min.js"></script>

    <![endif]-->
</head>
<body>
<header style="background-color: midnightblue;width: 100%;text-align: left;position: fixed;">
    <h3 style="color: white;margin: 0;padding: 1% 5%;">Atomic Project</h3>
</header>

<div id="wrapper">
    <aside id="sideBar" style="margin-top: 4%">
        <ul class="main-nav">
            <li>
                <a href="../BookTitle/create.php">- Book Title</a>
            </li>
            <li>
                <a href="create.php">- Birthday</a>
            </li>
            <li>
                <a href="../City/create.php">- City</a>
            </li>
            <li>
                <a href="../Email/create.php">- Email</a>
            </li>
            <li>
                <a href="../Gender/create.php">- Gender</a>
            </li>
            <li>
                <a href="../Hobbies/create.php">- Hobbies</a>
            </li>
            <li>
                <a href="../Profile_Picture/create.php">- Profile Picture</a>
            </li>
            <li>
                <a href="../Summary_Of_Organization/create.php">- Summary of Organization</a>
            </li>
        </ul>
    </aside>
</div>-->






<form action="update.php" method="POST" class="form-horizontal" style="padding-top: 10%;">
    <fieldset>

        <!-- Form Name -->
        <legend><h3 class="text-center">Birthday Form</h3></legend>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="id">Name</label>
            <div class="col-md-4">
                <input type="text" id="id" name="id" value="<?php echo $onedata->id; ?>" class="form-control input-md" readonly="readonly">
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="user_name">Name</label>
            <div class="col-md-4">
                <input type="text" id="user_name" name="user_name" value="<?php echo $onedata->name; ?>" class="form-control input-md" required="required">
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="birth_date">Birth date</label>
            <div class="col-md-4">
                <input type="date" id="birth_date" name="birth_date" value="<?php echo $onedata->birth_date; ?>" class="form-control input-md" required="required">

            </div>
        </div>

        <!-- Button -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="submit"></label>
            <div class="col-md-4">
                <button type="submit" id="submit" name="submit" class="btn btn-info">update</button>
                <a href="index.php"><button class="btn btn-danger">cancel</button></a>
            </div>
        </div>
        <div id="Mynote">
            <?php
                echo Message::message();
            ?>
        </div>
    </fieldset>
</form>
<script>
    $(document).ready(function(){
        $("#Mynote").fadeOut("slow");
    })
</script>
</body>
</html>