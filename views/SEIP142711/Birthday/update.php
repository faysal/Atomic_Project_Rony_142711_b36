<?php
require_once ("../../../vendor/autoload.php");
use App\Birthday\Birthday;
if(!isset($_SESSION)){
    session_start();
}

$objBirthday = new Birthday();
$objBirthday->setData($_POST);
$objBirthday->update();