<?php
require_once ("../../../vendor/autoload.php");
use App\Birthday\Birthday;

$objBirthday = new Birthday();
$objBirthday->setData($_GET);
$oneData = $objBirthday->view("obj");
?>

<!doctype html>
<html>
<head>
    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/Birthday_assets/js/jquery.min.js"></script>
    <script src="../../../resource/Birthday_assets/js/bootstrap.min.js"></script>
</head>
<body>
<header style="text-align: center">
    <h3>Data</h3>
</header>
<table class="table table-bordered" style="margin: 0 auto;width: 30%;">
    <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Birth Date</th>
        </tr>
    </thead>
    <tr>
        <td><?php echo $oneData->id; ?></td>
        <td><?php echo $oneData->name; ?></td>
        <td><?php echo $oneData->birth_date; ?></td>
    </tr>
</table>
<div style="text-align: center;margin: 2% auto;">
    <a href="index.php"><button class="btn btn-primary">back</button></a>
</div>
</body>
</html>
