<?php
require_once ("../../../vendor/autoload.php");
use App\Message\Message;
?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Summary of Organization</title>

    <!--sidebar links-->
    <link rel="stylesheet" href="../../../resource/Birthday_assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/Birthday_assets/css/font-awesome.min.css">
    <link href="../../../resource/Birthday_assets/css/main.css" rel="stylesheet">
    <!-- sidebar links end -->

    <!-- form links start -->
    <script src="../../../resource/Birthday_assets/js/bootstrap.min.js"></script>
    <script src="../../../resource/Birthday_assets/js/jquery-1.11.1.min.js"></script>
    <script src="../../../resource/Birthday_assets/js/jquery.min.js"></script>
    <link href="../../../resource/Birthday_assets/css/bootstrap-theme.css" rel="stylesheet">
    <!-- form links end -->
    
    
</head>
<body background="../../../img/12.jpg">
<header style="background-color: black;width: 100%;text-align: left;position: fixed;">
    <h3 style="color: white;margin: 0;padding: 1% 5%;">Atomic Project</h3>
</header>
    <div id="wrapper">
        <aside id="sideBar" style="margin-top: 4%;">
            <ul class="main-nav">
                <li>
                    <a href="../BookTitle/create.php">- Book Title</a>
                </li>
                <li>
                    <a href="../Birthday/create.php">- Birthday</a>
                </li>
                <li>
                    <a href="../City/create.php">- City</a>
                </li>
                <li>
                    <a href="../Email/create.php">- Email</a>
                </li>
                <li>
                    <a href="../Gender/create.php">- Gender</a>
                </li>
                <li>
                    <a href="../Hobbies/create.php">- Hobbies</a>
                </li>
                <li>
                    <a href="../Profile_Picture/create.php">- Profile Picture</a>
                </li>
                <li>
                    <a href="create.php">- Summary of Organization</a>
                </li>
            </ul>
        </aside>
    </div>

<form action="store.php" method="post" class="form-horizontal" style="padding-top: 10%;" >
    <fieldset>

        <!-- Form Name -->
        <legend><h3 class="text-center">Organization Form</h3></legend>

        <!-- name-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="short_name">Short Name</label>
            <div class="col-md-4">
                <input type="text" id="short_name" name="short_name" placeholder="Enter short name...." class="form-control input-md" required="required">
            </div>
        </div>

        <!-- name-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="full_name">Full Name</label>
            <div class="col-md-4">
                <input type="text" id="full_name" name="full_name" placeholder="Enter full name...." class="form-control input-md" required="required">
            </div>
        </div>

        <!-- name-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="task">Task</label>
            <div class="col-md-4">
                <input type="text" id="task" name="task" placeholder="Enter name...." class="form-control input-md" required="required">
            </div>
        </div>



        <!-- Button -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="save"></label>
            <div class="col-md-4">
                <button type="submit" id="submit" name="submit" class="btn btn-info">save</button>
            </div>
        </div>
        <a style="color:black;margin-left: 435px;text-decoration: none" href="index.php">show list</a>
        <div id="Mynote" style="margin-left: 34%;">
            <p><b><?php echo Message::message(); ?></b></p>
        </div>
    </fieldset>
</form>

</body>
</html>