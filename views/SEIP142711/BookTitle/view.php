<?php
require_once("../../../vendor/autoload.php");
use App\BookTitle\BookTitle;
$objBookTitle  =  new BookTitle();
$objBookTitle->setData($_GET);
$oneData= $objBookTitle->view();  //by default assoc..

//if assoc then
echo "<table style='border: 2px solid black'>
        <tr>
            <th>Id</th>
            <th>Book Title</th>
            <th>Author Name</th>
            <th>Price</th>
        </tr>";
    echo "<tr>";
        echo "<td>".$oneData['id']."</td>";
        echo "<td>".$oneData['book_title']."</td>";
        echo "<td>".$oneData['author_name']."</td>";
        echo "<td>".$oneData['price']."</td>";
    echo "</tr>";
echo "</table>";

/*$oneData= $objBookTitle->view("obj");*/
//if object then

/*echo "ID: ".$oneData->id."<br>";
echo "Book Title: ".$oneData->book_title."<br>";
echo "Author Name: ".$oneData->author_name."<br>";
echo "Price: ".$oneData->price."<br>";*/
?>