<?php
require_once("../../../vendor/autoload.php");
use App\BookTitle\BookTitle;

$objBookTitle = new BookTitle();
$allData = $objBookTitle->index("obj");

######################## pagination code block#1 of 2 start ######################################
$recordCount= count($allData);

if(isset($_REQUEST['Page']))
    $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))
    $page = $_SESSION['Page'];
else
    $page = 1;
$_SESSION['Page']= $page;

if(isset($_REQUEST['ItemsPerPage']))
    $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))
    $itemsPerPage = $_SESSION['ItemsPerPage'];
else
    $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;

$pages = ceil($recordCount/$itemsPerPage);
$someData = $objBookTitle->indexPaginator($page,$itemsPerPage);

$serial = (($page-1) * $itemsPerPage) +1;

####################### pagination code block#1 of 2 end #########################################


?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/Birthday_assets/js/jquery.min.js"></script>
    <script src="../../../resource/Birthday_assets/js/bootstrap.min.js"></script>
</head>
<body>
    <header style="text-align: center">
        <h3>Book Title list</h3>
    </header>
    <table class="table" style="width: 75%;margin: 2% auto;">
        <thead>
        <tr>
            <th>Serial No.</th>
            <th>Id</th>
            <th>Book Title</th>
            <th>Author Name</th>
            <th>Price</th>
            <th>Action</th>
        </tr>
        </thead>

<?php
//$serial=1;
foreach($allData as $oneData){
?>
    <tr class="table">
        <td><?php echo $serial; ?></td>
        <td><?php echo $oneData->id; ?></td>
        <td><?php echo $oneData->book_title; ?></td>
        <td><?php echo $oneData->author_name; ?></td>
        <td><?php echo $oneData->price; ?></td>
        <td>
            <a href="view.php?id=<?php echo $oneData->id; ?>"><button class="btn btn-default">view</button></a>
            <a href="edit.php?id=<?php echo $oneData->id; ?>"><button class="btn btn-primary">edit</button></a>
            <a href="delete.php?id=<?php echo $oneData->id; ?>"><button id="delete" class="btn btn-danger">delete</button></a>
            <a href="trash_delete.php?id=<?php echo $oneData->id; ?>"><button class="btn btn-primary">Trash delete</button></a>
        </td>
    </tr>
<?php
    $serial++;
}
?>
    </table>
<div style="text-align: center">
    <a href="create.php"><button class="btn btn-primary" style="margin-bottom: 15px;">insert</button></a>
    <a href="trashed_list.php"><button class="btn btn-danger" style="margin-bottom: 15px;">trashed list</button></a>
</div>



<!--  ######################## pagination code block#2 of 2 start ###################################### -->
<div align="center" class="container">
    <ul class="pagination">

        <?php
            $pageMinusOne  = $page-1;
            $pagePlusOne  = $page+1;
            if($page>$pages) 
                Utility::redirect("index.php?Page=$pages");
            if($page>1)
                echo "<li><a href='index.php?Page=$pageMinusOne'>" . "Previous" . "</a></li>";
            for($i=1;$i<=$pages;$i++) {
                if($i==$page) 
                    echo '<li class="active"><a href="">'. $i . '</a></li>';
                else  
                    echo "<li><a href='?Page=$i'>". $i . '</a></li>';
            }
            if($page<$pages)
                echo "<li><a href='index.php?Page=$pagePlusOne'>" . "Next" . "</a></li>";

        ?>

        <select  class="form-control"  name="ItemsPerPage" id="ItemsPerPage" onchange="javascript:location.href = this.value;" >
            <?php
            if($itemsPerPage==3 )
                echo '<option value="?ItemsPerPage=3" selected >Show 3 Items Per Page</option>';
            else
                echo '<option value="?ItemsPerPage=3">Show 3 Items Per Page</option>';

            if($itemsPerPage==4 )
                echo '<option value="?ItemsPerPage=4" selected >Show 4 Items Per Page</option>';
            else
                echo '<option value="?ItemsPerPage=4">Show 4 Items Per Page</option>';

            if($itemsPerPage==5 )
                echo '<option value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
            else
                echo '<option value="?ItemsPerPage=5">Show 5 Items Per Page</option>';
            ?>
        </select>
    </ul>
</div>
<!--  ######################## pagination code block#2 of 2 end ###################################### --!>

</body>
</html>
