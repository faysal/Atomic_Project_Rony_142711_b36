<?php
require_once ("../../../vendor/autoload.php");
use App\BookTitle\BookTitle;
if(!isset($_SESSION)){
    session_start();
}
$objBooktitle = new BookTitle();
$objBooktitle->setData($_GET);
$objBooktitle->recover();