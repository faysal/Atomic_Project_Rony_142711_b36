<?php
require_once ("../../../vendor/autoload.php");
use App\BookTitle\BookTitle;
use App\Message\Message;
if(!isset($_SESSION)){
    session_start();
}

$objBooktitle = new BookTitle();
$objBooktitle->setData($_POST);
$objBooktitle->update();