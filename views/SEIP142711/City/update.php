<?php
require_once ("../../../vendor/autoload.php");
use App\City\City;
if(!isset($_SESSION)){
    session_start();
}
$objCity = new City();
$objCity->setData($_POST);
$objCity->update();