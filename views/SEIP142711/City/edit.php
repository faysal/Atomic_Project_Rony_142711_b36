<?php
require_once ("../../../vendor/autoload.php");
use App\City\City;

$objCity = new City();
$objCity->setData($_GET);
$onedata = $objCity->show("obj");
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>BookTitle</title>

    <!--sidebar links-->
    <link rel="stylesheet" href="../../../resource/Birthday_assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/Birthday_assets/css/font-awesome.min.css">
    <link href="../../../resource/Birthday_assets/css/main.css" rel="stylesheet">
    <!-- sidebar links end -->

    <!-- form links start -->
    <script src="../../../resource/Birthday_assets/js/bootstrap.min.js"></script>
    <script src="../../../resource/Birthday_assets/js/jquery-1.11.1.min.js"></script>
    <script src="../../../resource/Birthday_assets/js/jquery.min.js"></script>
    <link href="../../../resource/Birthday_assets/css/bootstrap-theme.css" rel="stylesheet">
    <!-- form links end -->

</head>
<body>
<header style="background-color: midnightblue;width: 100%;text-align: left;position: fixed;">
    <h3 style="color: white;margin: 0;padding: 1% 5%;">Atomic Project</h3>
</header>
<div id="wrapper">
    <aside id="sideBar" style="margin-top: 4%">
        <ul class="main-nav">
            <li>
                <a href="create.php">- Book Title</a>
            </li>
            <li>
                <a href="../Birthday/create.php">- Birthday</a>
            </li>
            <li>
                <a href="../City/create.php">- City</a>
            </li>
            <li>
                <a href="../Email/create.php">- Email</a>
            </li>
            <li>
                <a href="../Gender/create.php">- Gender</a>
            </li>
            <li>
                <a href="../Hobbies/create.php">- Hobbies</a>
            </li>
            <li>
                <a href="../Profile_Picture/create.php">- Profile Picture</a>
            </li>
            <li>
                <a href="../Summary_Of_Organization/create.php">- Summary of Organization</a>
            </li>
        </ul>
    </aside>
</div>

<form class="form-horizontal" style="padding-top: 10%;" action="update.php" method="POST">
    <fieldset>

        <!-- Form Name -->
        <legend><h3 class="text-center">City Form</h3></legend>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="id">Id</label>
            <div class="col-md-4">
                <input type="text" id="id" name="id" value="<?php echo $onedata->id; ?>" class="form-control input-md" readonly="readonly">
            </div>
        </div>

        <!-- name-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="name">Name</label>
            <div class="col-md-4">
                <input type="text" id="name" name="name" value="<?php echo $onedata->name; ?>" class="form-control input-md" required="required">
            </div>
        </div>

        <!-- city-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="city">Author Name</label>
            <div class="col-md-4">
                <!--<input type="text" id="city" name="city" placeholder="enter city...." class="form-control input-md" required="required">-->
                <select id="city" name="city" class="form-control input-md">
                    <option value="<?php echo $onedata->city; ?>"><?php echo $onedata->city; ?></option>
                    <?php
                        if($onedata->city != "Chittagong"){
                    ?>
                        <option value="Chittagong">Chittagong</option>
                    <?php
                        }
                    ?>
                    <?php
                    if($onedata->city != "Dhaka"){
                    ?>
                        <option value="Dhaka">Dhaka</option>
                    <?php
                    }
                    ?>
                    <?php
                    if($onedata->city != "Commilla"){
                    ?>
                        <option value="Commilla">Commilla</option>
                    <?php
                    }
                    ?>
                </select>
            </div>
        </div>

        <!-- Button -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="save"></label>
            <div class="col-md-4">
                <button type="submit" id="submit" name="submit" class="btn btn-info">update</button>
            </div>
        </div>

    </fieldset>
</form>
</body>
</html>