<?php
require_once("../../../vendor/autoload.php");
use App\City\City;

$objCity = new City();
$allData = $objCity->index("obj");
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/Birthday_assets/js/jquery.min.js"></script>
    <script src="../../../resource/Birthday_assets/js/bootstrap.min.js"></script>
</head>
<body>
<header style="text-align: center">
    <h3>City list</h3>
    <a href="create.php"><button class="btn btn-primary">insert</button></a>
</header>
<table class="table" style="width: 60%;margin: 2% auto;">
    <thead>
    <tr>
        <th>Serial No.</th>
        <th>Id</th>
        <th>Name</th>
        <th>City</th>
        <th>Action</th>
    </tr>
    </thead>

    <?php
    $serial = 1;
    foreach($allData as $oneData){
        ?>
        <tr class="table">
            <td><?php echo $serial; ?></td>
            <td><?php echo $oneData->id; ?></td>
            <td><?php echo $oneData->name; ?></td>
            <td><?php echo $oneData->city; ?></td>
            <td>
                <a href="view.php?id=<?php echo $oneData->id; ?>"><button class="btn btn-default">view</button></a>
                <a href="edit.php?id=<?php echo $oneData->id; ?>"><button class="btn btn-primary">edit</button></a>
                <a href="delete.php?id=<?php echo $oneData->id; ?>"><button id="delete" class="btn btn-danger">delete</button></a>
                <a href="trash_delete.php?id=<?php echo $oneData->id; ?>"><button class="btn btn-primary">Trash delete</button></a>
            </td>
        </tr>
        <?php
        $serial++;
    }
    ?>
</table>
<div style="text-align: center">
    <a href="create.php"><button class="btn btn-primary" style="margin-bottom: 15px;">insert</button></a>
    <a href="trashed_list.php"><button class="btn btn-danger" style="margin-bottom: 15px;">trashed list</button></a>
</div>
</body>
</html>
