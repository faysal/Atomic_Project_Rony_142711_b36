<?php
require_once("../../../vendor/autoload.php");
use App\Profile_Picture\Profile_Picture;
if(!isset($_SESSION)){
    session_start();
}

$objPicture = new Profile_Picture();
$allData = $objPicture->index("obj");
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/Birthday_assets/js/jquery.min.js"></script>
    <script src="../../../resource/Birthday_assets/js/bootstrap.min.js"></script>
</head>
<body>
<header style="text-align: center">
    <h3>Picture list</h3>
    <a href="create.php"><button class="btn btn-primary">insert</button></a>
</header>
<table class="table" style="width: 65%;margin: 2% auto;">
    <thead>
    <tr>
        <th>Serial No.</th>
        <th>Id</th>
        <th>Name</th>
        <th>Picture</th>
        <th style="text-align: center">Action</th>
    </tr>
    </thead>

    <?php
    $serial = 1;
    foreach($allData as $oneData){
        $path = "../picture/".$oneData->profile_picture;
        ?>
        <tr class="table">
            <td><?php echo $serial; ?></td>
            <td><?php echo $oneData->id; ?></td>
            <td><?php echo $oneData->name; ?></td>
            <td><img style="height: 100px;width: 200px;" src=<?php echo $path; ?>></td>
            <td>
                <a href="view.php?id=<?php echo $oneData->id; ?>"><button class="btn btn-default">view</button></a>
                <a href="edit.php?id=<?php echo $oneData->id; ?>"><button class="btn btn-primary">edit</button></a>
                <a href="delete.php?id=<?php echo $oneData->id; ?>"><button id="delete" class="btn btn-danger">delete</button></a>
                <a href="trash_delete.php?id=<?php echo $oneData->id; ?>"><button class="btn btn-primary">Trash delete</button></a>
            </td>
        </tr>
        <?php
        $serial++;
    }
    ?>
</table>
</body>
</html>
