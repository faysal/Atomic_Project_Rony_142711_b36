<?php
require_once ("../../../vendor/autoload.php");
use App\Profile_Picture\Profile_Picture;
if(!isset($_SESSION)){
    session_start();
}

$objPicture = new Profile_Picture();
$objPicture->setData($_GET);
$objPicture->delete();